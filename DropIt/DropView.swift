//
//  DropView.swift
//  DropIt
//
//  Created by Marcos Gonzalez on 12/07/16.
//  Copyright © 2016 Mobgen. All rights reserved.
//

import UIKit
import CoreMotion

class DropView: NamedBezierPathsView, UIDynamicAnimatorDelegate  {
    
    private lazy var animator:UIDynamicAnimator = {
       
        let animator = UIDynamicAnimator(referenceView: self)
        animator.delegate = self
        
        return animator
    }()
    
    func dynamicAnimatorDidPause(animator: UIDynamicAnimator) {
        
        removeCompletedRow()
    }
    
    private let fallingBehavior = FallingBehavior()
    
    var animating = false {
        
        didSet {
            
            if animating {
                
                animator.addBehavior(fallingBehavior)
                updateRealGravity()
            }else {
                
                animator.removeBehavior(fallingBehavior)
            }
        }
    }
    
    private let dropsPerRow = 10
    
    private var dropSize: CGSize {
        
        let size = bounds.size.width / CGFloat(self.dropsPerRow)
        return CGSize(width: size, height: size)
    }
    
    private var attachment: UIAttachmentBehavior? {
        
        willSet {
            
            if attachment != nil {
                
                animator.removeBehavior(attachment!)
                bezierPaths[PathNames.Attachments] = nil
            }
        }
        
        didSet {
            
            if attachment != nil {
                
                animator.addBehavior(attachment!)
                attachment!.action = { [unowned self] in
                    
                    if let attachedDrop = self.attachment?.items.first as? UIView {
                        
                        self.bezierPaths[PathNames.Attachments] = UIBezierPath.lineFrom(self.attachment!.anchorPoint, to: attachedDrop.center)
                    }
                }
            }
        }
    }
    
    private struct PathNames {
        
        static let MiddleBarrier = "Middle barrier"
        static let Attachments = "Attachments"
    }
    
    private let motionManager = CMMotionManager()
    
    var realGravity = false {
        
        didSet {
            
            updateRealGravity()
        }
    }
    
    private func updateRealGravity() {
        
        if realGravity {
            
            if motionManager.accelerometerAvailable && !motionManager.accelerometerActive{
                
                motionManager.accelerometerUpdateInterval = 0.25
                motionManager.startAccelerometerUpdatesToQueue(NSOperationQueue.mainQueue(), withHandler: { [unowned self] (data, error) in
                    
                    if self.fallingBehavior.dynamicAnimator != nil {
                        
                        if var dx = data?.acceleration.x, var dy = data?.acceleration.y {
                            
                            switch UIDevice.currentDevice().orientation {
                                
                            case .Portrait:
                                
                                dy = -dy
                            case .PortraitUpsideDown:
                                
                                break
                            case .LandscapeRight:
                                
                                swap(&dx, &dy)
                            case .LandscapeLeft:
                                
                                swap(&dx, &dy)
                                dy = -dy
                            default:
                                
                                dx = 0
                                dy = 0
                            }
                            
                            self.fallingBehavior.gravity.gravityDirection = CGVector(dx: dx, dy: dy)
                        }
                    }else {
                        
                        self.motionManager.stopAccelerometerUpdates()
                    }
                })
            }
        }else {
            
            motionManager.stopAccelerometerUpdates()
        }
    }
    
    override func layoutSubviews() {
        
        super.layoutSubviews()
        
        let path = UIBezierPath(ovalInRect: CGRect(center: bounds.mid, size: dropSize))
        fallingBehavior.addBarrier(path, named: PathNames.MiddleBarrier)
        bezierPaths[PathNames.MiddleBarrier] = path
    }
    
    func grabDrop(recognizer: UIPanGestureRecognizer) {
        
        let gesturePoint = recognizer.locationInView(self)
        
        switch recognizer.state {
            
        case .Began:
            //
            if let dropToAttachTo = lastDrop where dropToAttachTo.superview != nil {
                
                attachment = UIAttachmentBehavior(item: dropToAttachTo, attachedToAnchor: gesturePoint)
            }
            
            lastDrop = nil
        case .Changed:
            //
            attachment?.anchorPoint = gesturePoint
        default:
            attachment = nil
        }
     }
    
    private var lastDrop: UIView?
    
    func addDrop() {
        
        var frame = CGRect(origin: CGPointZero, size: dropSize)
        frame.origin.x = CGFloat.random(dropsPerRow) * dropSize.width
        
        let drop = UIView(frame: frame)
        drop.backgroundColor = UIColor.random
        
        addSubview(drop)
        fallingBehavior.addItem(drop)
        lastDrop = drop
    }
    
    private func removeCompletedRow() {
        
        var dropsToRemove = [UIView]()
        var hitTestRect = CGRect(origin: bounds.lowerLeft, size: dropSize)

        repeat {
            
            hitTestRect.origin.x = bounds.minX
            hitTestRect.origin.y -= dropSize.height
            
            var dropsTested = 0
            var dropsFound = [UIView]()
            
            while dropsTested < dropsPerRow {
                
                if let hitView = hitTest(hitTestRect.mid) where hitView.superview == self {
                    
                    dropsFound.append(hitView)
                }else {
                    
                    break
                }
                
                hitTestRect.origin.x += dropSize.width
                dropsTested += 1
            }
            
            if dropsTested == dropsPerRow {
                
                dropsToRemove += dropsFound
            }
            
        }while dropsToRemove.count == 0 && hitTestRect.origin.y > bounds.minY
        
        for drop in dropsToRemove {
            
            fallingBehavior.removeItem(drop)
            drop.removeFromSuperview()
        }
    }
}

extension CGFloat {
    
    static func random(max: Int) -> CGFloat {
        return CGFloat(arc4random() % UInt32(max))
    }
}

extension UIColor {
    
    static var random: UIColor {
        
        switch arc4random() % 5 {
            
        case 0: return UIColor.redColor()
        case 1: return UIColor.greenColor()
        case 2: return UIColor.orangeColor()
        case 3: return UIColor.blueColor()
        case 4: return UIColor.purpleColor()
        default: return UIColor.blackColor()
        }
    }
}

extension CGRect {
    
    var mid: CGPoint { return CGPoint(x: midX, y: midY) }
    var upperLeft: CGPoint { return CGPoint(x: minX, y: minY) }
    var lowerLeft: CGPoint { return CGPoint(x: minX, y: maxY) }
    var upperRight: CGPoint { return CGPoint(x: maxX, y: minY) }
    var lowerRight: CGPoint { return CGPoint(x: maxX, y: maxY) }
    
    init(center: CGPoint,  size: CGSize) {
        
        let upperLeft = CGPoint(x: center.x-size.width/2, y: center.y-size.height/2)
        self.init(origin: upperLeft, size: size)
    }
}

extension UIView {
    
    func hitTest(p: CGPoint) -> UIView? {
        
        return hitTest(p, withEvent: nil)
    }
}

extension UIBezierPath {
    
    class func lineFrom(from: CGPoint, to: CGPoint) -> UIBezierPath {
        
        let path = UIBezierPath()
        path.moveToPoint(from)
        path.addLineToPoint(to)
        
        return path
    }
}
