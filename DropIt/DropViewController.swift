//
//  DropViewController.swift
//  DropIt
//
//  Created by Marcos Gonzalez on 12/07/16.
//  Copyright © 2016 Mobgen. All rights reserved.
//

import UIKit

class DropViewController: UIViewController {

    @IBOutlet weak var gameView: DropView! {
        
        didSet {
            
            gameView.addGestureRecognizer( UITapGestureRecognizer(target: self, action: #selector(DropViewController.addDrop(_:)) ))
            gameView.addGestureRecognizer( UIPanGestureRecognizer(target: gameView , action: #selector(DropView.grabDrop(_:)) ))
            gameView.realGravity = true
        }
    }

    func addDrop(recognizer: UITapGestureRecognizer) {
        
        if recognizer.state == .Ended {
            
            gameView.addDrop()
        }
    }

    override func viewDidAppear(animated: Bool) {
        
        super.viewDidAppear(animated)
        
        gameView.animating = true
    }
    
    override func viewWillDisappear(animated: Bool) {
        
        super.viewWillDisappear(animated)
        
        gameView.animating = false
    }
}
