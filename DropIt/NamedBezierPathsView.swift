//
//  NamedBezierPathsView.swift
//  DropIt
//
//  Created by Marcos Gonzalez on 13/07/16.
//  Copyright © 2016 Mobgen. All rights reserved.
//

import UIKit

class NamedBezierPathsView: UIView {
    
    var bezierPaths = [String:UIBezierPath]() {
        
        didSet {
            
            setNeedsDisplay()
        }
    }
    
    override func drawRect(rect: CGRect) {
        
        for (_, path) in bezierPaths {
            
            path.stroke()
        }
    }
}
